/* Copyright (c) 2015 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package poet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import graph.ConcreteVerticesGraph;

/***
 * A graph-based poetry generator.
 * 
 * <p>GraphPoet is initialized with a corpus of text, which it uses to derive a
 * word affinity graph.
 * Vertices in the graph are words. Words are defined as non-empty
 * case-insensitive strings of non-space non-newline characters.
 * Edges in the graph count adjacencies: the number of times "w1" is followed by
 * "w2" in the corpus is the weight of the edge from w1 to w2.
 * 
 * <p>For example, given this corpus:
 * <pre>    Hello, HELLO, hello, goodbye!    </pre>
 * <p>the graph would contain two edges:
 * <ul><li> ("hello,") -> ("hello,")   with weight 2
 *     <li> ("hello,") -> ("goodbye!") with weight 1 </ul>
 * <p>where the vertices represent case-insensitive {@code "hello,"} and
 * {@code "goodbye!"}.
 * 
 * <p>Given an input sentence, GraphPoet generates a poem by attempting to
 * insert a bridge word between every adjacent pair of words in the input.
 * The bridge word between input words "w1" and "w2" will be some "b" such that
 * w1 -> b -> w2 is a two-edge-long path with maximum-weight weight among all
 * the two-edge-long paths from w1 to w2 in the affinity graph.
 * If there are no such paths, no bridge word is inserted.
 * In the output poem, input words retain their original case, while bridge
 * words are lower case. The whitespace between every word in the poem is a
 * single space.
 * 
 * <p>For example, given this corpus:
 * <pre>    This is a test of the Mugar Omni Theater sound system.    </pre>
 * <p>on this input:
 * <pre>    Test the system.    </pre>
 * <p>the output poem would be:
 * <pre>    Test of the system.    </pre>
 * 
 * <p>PS2 instructions: this is a required ADT class, and you MUST NOT weaken
 * the required specifications. However, you MAY strengthen the specifications
 * and you MAY add additional methods.
 * You MUST use Graph in your rep, but otherwise the implementation of this
 * class is up to you.
 */
public class GraphPoet {
    
    private final ConcreteVerticesGraph<String> graph = new ConcreteVerticesGraph<String>();
    
    // Abstraction function:
    //   A literary work described by a graph where each node in the graph is a unique word in the work
    //   Each edge in the graph is weighted with the amount of times a specific word follows another in the input.
    // Representation invariant:
    //   word is a source or target for another word
    // Safety from rep exposure:
    //   - only field is the graph which is kept private
    //   - graph is only modified by constructor method
    
    /**
     * Create a new poet with the graph from corpus (as described above).
     * 
     * @param corpus text file from which to derive the poet's affinity graph
     * @throws IOException if the corpus file cannot be found or read
     */
    public GraphPoet(File corpus) throws IOException {
        FileReader myReader = new FileReader(corpus);
        BufferedReader bReader = new BufferedReader(myReader);
        String line = new String();
        List<String> words = new ArrayList<String>();
        String previousWord = "a";
        String currentWord = "a";
        Boolean firstWord = true;
        while ((line = bReader.readLine()) != null){
            words = Arrays.asList(line.split(" "));
            for (int w = 0; w < words.size(); w++){
                checkRep();
                if (firstWord){
                    firstWord = false;
                    previousWord = words.get(w).toLowerCase();
                    graph.add(previousWord);
                }
                else{
                    graph.add(previousWord);
                    graph.add(currentWord);
                    if (!words.get(w).equals("") && !words.get(w).equals("\n")) currentWord = words.get(w).toLowerCase();
                    if (graph.targets(previousWord).containsKey(currentWord)){
                        int weight = graph.targets(previousWord).get(currentWord);
                        graph.set(previousWord, currentWord, weight + 1);
                    }
                    else{
                        graph.set(previousWord, currentWord, 1);
                    }
                    previousWord = currentWord;    
                }
            }           
        }
        bReader.close();
    }
    
    private void checkRep(){
        //TODO: change assert to return true/false
        for (String vertex : graph.vertices()){
            for (String key : graph.targets(vertex).keySet()){
                assert(graph.vertices().contains(key));
            }
            for (String key : graph.sources(vertex).keySet()){
                assert(graph.vertices().contains(key));
            }
        }
    }
    
    /**
     * Generate a poem.
     * 
     * @param input string from which to create the poem
     * @return poem (as described above)
     */
    public String poem(String input) {
        List<String> poemList = new ArrayList<String>(Arrays.asList(input.split(" ")));
        for (int w = 0; w < poemList.size(); w++){
            if (poemList.get(w).equals("\n") || poemList.get(w).equals("")){
                poemList.remove(w);
                if (w != poemList.size()) w = -1; 
            }
        }
        String currentWordOC;
        String currentWord;
        String previousWord;
        String bridgeWord = "a";
        StringBuilder output = new StringBuilder();
        int maxWeight = 0;  
        Boolean bridged;
        Map<String, Integer> targets1 = new HashMap<String, Integer>();    
        if (input.length() == 0) return new String();       
        if (input.length() == 1) return input;
        output.append(poemList.get(0));
        for (int w = 1; w < poemList.size(); w++) {
            bridged = false;
            maxWeight = 0;
            previousWord = poemList.get(w-1).toLowerCase();
            currentWordOC = poemList.get(w);
            currentWord = poemList.get(w).toLowerCase();           
            if (graph.vertices().contains(currentWord) && graph.vertices().contains(previousWord)) {
                targets1 = graph.targets(previousWord);
                for (String bridge : targets1.keySet()){
                    if (graph.targets(bridge).containsKey(currentWord)){
                        if (graph.targets(previousWord).get(bridge) + graph.targets(bridge).get(currentWord) > maxWeight){
                            maxWeight = graph.targets(previousWord).get(bridge) + graph.targets(bridge).get(currentWord);
                            bridgeWord = bridge;
                            bridged = true;
                        }
                    }
                }
            }
            if (bridged) output.append(" " + bridgeWord);
            output.append(" " + currentWordOC);
        }
        return output.toString();
    }
    
    /**
     * @return a string representation of the graph as determined by the implementation of graph being used.
     */
    public String toString(){
        return graph.toString();
    }
    
}
