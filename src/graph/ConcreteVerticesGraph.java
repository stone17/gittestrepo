/* Copyright (c) 2015 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package h23344;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * An implementation of Graph.
 * 
 * A graph that is represented as a list of vertices, where a vertex is described below.
 */
public class ConcreteVerticesGraph<L> implements Graph<L> {
    
    private final List<Vertex<L>> vertices = new ArrayList<Vertex<L>>();
    
    // Abstraction function:
    //   A directed graph as represented by a list of vertices of type VERTEX.
    // Representation invariant:
    //   Each vertex in the graph occurs exactly once in the graph, and a vertex is connected to each other 
    //   vertex with at most one edge
    // Safety from rep exposure:
    //   - list of vertices is private
    //   - vertices list is modified only with instance methods below
    
    /**
     * creates new ConcreteVerticesGraph
     */
    public ConcreteVerticesGraph(){}
    
    /**
     * @return boolean if current state of graph is within spec defined above
     */
    public void checkRep(){
        for (int v = 0; v < vertices.size(); v++){
            for (L key : vertices.get(v).getKeySet()){
                assert(vertices.get(v).getTarget(key) > 0);
            }
        }
    }
    
    @Override public boolean add(L vertex) {
        for (int v = 0; v < vertices.size(); v++){
            if (vertices.get(v).getName().equals(vertex)){
                checkRep();
                return false;
            }
        }
        Vertex<L> newVertex = new Vertex<L>(vertex, new HashMap<L, Integer>()) ;
        vertices.add(newVertex);
        checkRep();
        return true;
    }
    
    @Override public int set(L source, L target, int weight) {
        Vertex<L> currentVertex;
        for (int v = 0; v < vertices.size(); v++){
            currentVertex = vertices.get(v);
            if (currentVertex.getName().equals(source) && currentVertex.containsTarget(target)){
                int previousWeight = currentVertex.getTarget(target);
                if (weight == 0){
                    currentVertex.removeTarget(target);
                    checkRep();
                    return previousWeight;
                }
                else {
                    currentVertex.setTarget(target, weight);
                    checkRep();
                    return previousWeight;
                }
            }
        }
        if (weight != 0){
            add(source);
            add(target);
            for (int v = 0; v < vertices.size(); v++){
                if (vertices.get(v).getName().equals(source)){
                    vertices.get(v).setTarget(target, weight);
                }
            }
            checkRep();
            return 0;
        }        
        return 0;
    }
    
    @Override public boolean remove(L vertex) {
        Boolean returnValue = false;
        int vIndex = 0;
        for (int v = 0; v < vertices.size(); v++){
            if (vertices.get(v).containsTarget(vertex)){
                set(vertices.get(v).getName(), vertex, 0);
            }
            if (vertices.get(v).getName().equals(vertex)){
                vIndex = v;
                returnValue = true;
            }
        }
        if (returnValue){
        vertices.remove(vIndex);
        }
        return returnValue;
    }
    
    @Override public Set<L> vertices() {
        Set<L> vertexNames = new HashSet<L>();
        for (int v = 0; v < vertices.size(); v++){
            vertexNames.add(vertices.get(v).getName());
        }
        checkRep();
        return vertexNames;
    }
    
    @Override public Map<L, Integer> sources(L target) {
        Map<L, Integer> sources = new HashMap<L, Integer>();
        for (int v = 0; v < vertices.size(); v++){
            if (vertices.get(v).containsTarget(target)){
                sources.put(vertices.get(v).getName(), vertices.get(v).getTarget(target));
            }
        }
        checkRep();
        return sources;
    }
    
    @Override public Map<L, Integer> targets(L source) {
        Map<L, Integer> targets = new HashMap<L, Integer>();
        for (int v = 0; v < vertices.size(); v++){
            if (vertices.get(v).getName().equals(source)){
                for (L key : vertices.get(v).getKeySet()){
                    targets.put(key, vertices.get(v).getTarget(key));
                }
            }
        }
        checkRep();
        return targets;
    }
    
    /**
     * @return String where each vertex is calls its toString method on an individual line
     */
    public String toString(){
        StringBuilder builder = new StringBuilder();
        for (int v = 0; v < vertices.size(); v++){
            builder.append("\n" + vertices.get(v).toString());
        }
        return builder.toString();
    }
    
}

/**
 * Mutable.
 * This class is internal to the rep of ConcreteVerticesGraph.
 * 
 * A vertex represented by a vertex name and a map of targets where the each target is a key in the map whose
 * value is that of the weight of the edge connecting this vertex to that specific target.
 * <p>PS2 instructions: the specification and implementation of this class is
 * up to you.
 */
class Vertex<L> {
    
    private L name;
    private Map<L, Integer> targets = new HashMap<L, Integer>();
    
    // Abstraction function:
    //   A vertex of a graph represented by a map and a name of type L
    //   The map includes the targets of the vertex to the weights of each of the edges connecting them
    // Representation invariant:
    //   -every weight in targets is non-0
    // Safety from rep exposure:
    //  - the map is private
    //  - and is only modified / observed by instance methods
    //  - name is type L - immutable
    
    public Vertex(L inputName, Map<L,Integer> inputTargets){
        targets = inputTargets;
        name = inputName;
    }
    
    /**
     * Public method for retrieving name of vertex.
     * @return L name of vertex
     */
    public L getName(){
        return name;
    }
    
    /**
     * Get value of weight ofedge to target.
     * @param L target that is connected to this vertex
     * @return the weight of the edge connecting from this vertex to the target
     */
    public Integer getTarget(L target){
        return new Integer(targets.get(target));
    }
    
    /**
     * Add a vertex as a target with a specific weight.
     * @param target - vertex to add as a target
     * @param value  - weight of the edge
     */
    public void setTarget(L target, Integer value){
        targets.put(target, value);
    }
    
    /**
     * Remove a specific vertex as a target
     * @param target - L vertex to remove
     */
    public void removeTarget(L target){
        targets.remove(target);
    }
    
    /**
     * Retrieve all the targets of this vertex.
     * @return the set of vertices targeted by this vertex
     */
    public Set<L> getKeySet(){
        return new HashSet<L>(targets.keySet());
    }
    
    /**
     * Public method for determining whether a vertex is a target of this vertex
     * @param target - L vertex that we want to know its status asa targer
     * @return true if target input is a target of this vertex
     */
    public boolean containsTarget(L target){
        return targets.containsKey(target);
    }
    
    /**
     * verify the vertex is within spec as described above
     */
    public void checkRep(){
        for ( L target : targets.keySet()){
            assert (targets.get(target) != 0);
        }
    }
    
    /**
     * @return String where a vertex is printed, followed by a semicolon and the set of its targets.
     */
    public String toString(){
        return new String(name  + ": " + targets);
    }
    
}
